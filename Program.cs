﻿using System;
using System.Diagnostics;
using System.Linq;
namespace Kata5_ReductoMultiplicitum
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Process.Start("explorer.exe", $"{System.IO.Directory.GetCurrentDirectory()}");
        }

        public static int SumDigProd(params int[] input)
        {
            //Add together input values
            int total = input.Sum();

            while (total >= 10)
            {
                //Set to one for multiplication purpose. 0 always gives 0.
                int newTotal = 1;

                //Product of digits
                for (int i = 0; i < total.ToString().Length; i++)
                {
                    //reduce 48 from Char value to get numeric value instead of Unicode
                    newTotal *= Convert.ToInt32(total.ToString().ToCharArray()[i] - 48);
                }
                total = newTotal;
            }
            return total;
        }
    }
}
